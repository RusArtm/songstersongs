# README #
## Project Description ##

The purpose of this project is share some code samples.

This test app is made for [www.songsterr.com](https://www.songsterr.com/)

Network requests done with Google recommended `Volley` library.

Features:

 * Uses remote api to get songs list and revision lists
 * Two simple activities with `ViewModel`s
 * Able to search songs through api
 * Shows a song revisions (loaded with api)
 * Project has one `Custom View` - `DifficultyView` that mimics the look of difficulty indicator from the site. Difficulty is calculated as average of track with non-zero difficulty. This is not how it is shown on the site. For songs with `id % 10 == 0` difficulty is set to `0` to show the look of `DifficultyView` for the case.
 * Responses are processed in threads. To change delay for these threads, set value to `NetworkManger.threadDebugDelay`. `0` will disable it

No local cache as this is a simple project.
Level of abstraction is not high here as this project is small and more abstractions would only complicate it.

## Installation ##

 1. Clone the repository
 1. Open the project with Android Studio

## Some Screenshots ##

Songs list  
![Songs List](1_SongsList.png)

Songs search  
![Songs List](2_SearchDialog.png)

Songs search  
![Revisions List](3_RevisionsList.png)

