package ru.atomarsoft.songstersongs.activities

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import ru.atomarsoft.songstersongs.R
import ru.atomarsoft.songstersongs.adapters.GeneralViewHolder
import ru.atomarsoft.songstersongs.adapters.SongsAdapter
import ru.atomarsoft.songstersongs.api.LoadSongsRequest
import ru.atomarsoft.songstersongs.api.NetworkManager
import ru.atomarsoft.songstersongs.model.DataStatus
import ru.atomarsoft.songstersongs.model.Song

class MainActivity : AppCompatActivity() {

    private lateinit var songsListView: RecyclerView
    private lateinit var songsListAdapter: SongsAdapter
    private lateinit var statusTextView: TextView
    private var menuItemRefresh: MenuItem? = null
    private var menuItemSearch: MenuItem? = null

    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModelProvider = ViewModelProvider(this)
        viewModel = viewModelProvider.get(MainActivityViewModel::class.java)

        songsListView = findViewById(R.id.songsList)
        songsListAdapter = SongsAdapter(songsListView, viewModel.songs)
        statusTextView = findViewById(R.id.statusTextView)

        songsListAdapter.setItemClickListener(object : GeneralViewHolder.ClickListener<Song> {
            override fun onClick(item: Song) {
                RevisionsActivity.launch(this@MainActivity, item)
            }
        })

        viewModel.status.observe(
            this,
            { state ->
                songsListAdapter.notifyDataSetChanged()
                when (state) {
                    DataStatus.Loading -> {
                        setMenuItemsEnabled(false)
                        statusTextView.text = getString(R.string.strLoading)
                    }
                    DataStatus.Ready -> {
                        setMenuItemsEnabled(true)
                        statusTextView.text =
                            String.format(getString(R.string.strSongsLoaded), viewModel.songs.size)
                    }
                    DataStatus.Error -> {
                        setMenuItemsEnabled(true)
                        statusTextView.text = getString(R.string.strErrorLoadingSongs)
                    }
                    else -> statusTextView.text = getString(R.string.strInitializing)
                }
            }
        )

        if (viewModel.status.value == DataStatus.None) reloadSongList()
        if (viewModel.searchDialogIsShown) showSearchDialog()
    }

    private fun reloadSongList() {
        viewModel.status.value = DataStatus.Loading
        NetworkManager.getApiRequestResult(LoadSongsRequest(
            viewModel.searchString,
            50,
            { list ->
                viewModel.songs.clear()
                viewModel.songs.addAll(list)
                viewModel.status.value = DataStatus.Ready
            },
            {
                viewModel.status.value = DataStatus.Error
            }
        ))
    }

    private fun setMenuItemsEnabled(enabled: Boolean) {
        val color = if (enabled) Color.WHITE else Color.GRAY
        changeMenuItemIconColor(menuItemRefresh, color)
        changeMenuItemIconColor(menuItemSearch, color)
        menuItemRefresh?.isEnabled = enabled
        menuItemSearch?.isEnabled = enabled
    }

    private fun changeMenuItemIconColor(menuItem: MenuItem?, color: Int) {
        menuItem?.icon?.colorFilter =
            BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                color,
                BlendModeCompat.SRC_ATOP
            )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        menuItemRefresh = menu.findItem(R.id.action_refresh)
        menuItemSearch = menu.findItem(R.id.action_search)

        setMenuItemsEnabled(
            viewModel.status.value == DataStatus.Ready
                    || viewModel.status.value == DataStatus.Error
        )

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh -> {
                reloadSongList()
                true
            }
            R.id.action_search -> {
                showSearchDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showSearchDialog() {
        viewModel.searchDialogIsShown = true
        val content = layoutInflater.inflate(R.layout.search_dialog, null)

        val edit = content.findViewById<EditText>(R.id.editSearchText)
        edit.setText(viewModel.searchString)

        val builder = AlertDialog.Builder(this)
        builder
            .setTitle(getString(R.string.strSearchTitle))
            .setPositiveButton(
                R.string.strSearch
            ) { dialog, _ ->
                viewModel.searchString = edit.text.toString()
                dialog.dismiss()
                reloadSongList()
            }
            .setNegativeButton(
                R.string.strCancel
            ) { dialog, _ ->
                dialog.dismiss()
            }
            .setOnDismissListener() {
                viewModel.searchDialogIsShown = false
            }
            .setIcon(R.drawable.baseline_search_black_24dp)
            .setView(content)
        builder.create().show()
    }

}
