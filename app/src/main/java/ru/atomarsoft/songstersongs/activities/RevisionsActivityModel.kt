package ru.atomarsoft.songstersongs.activities

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.atomarsoft.songstersongs.model.DataStatus
import ru.atomarsoft.songstersongs.model.Revision

class RevisionsActivityModel : ViewModel() {

    val status: MutableLiveData<DataStatus> = MutableLiveData(DataStatus.None)
    val revisions: MutableList<Revision> = mutableListOf()
    var songId: Int = -1

}
