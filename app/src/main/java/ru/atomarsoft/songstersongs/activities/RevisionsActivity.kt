package ru.atomarsoft.songstersongs.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import ru.atomarsoft.songstersongs.R
import ru.atomarsoft.songstersongs.adapters.RevisionAdapter
import ru.atomarsoft.songstersongs.api.LoadRevisionsRequest
import ru.atomarsoft.songstersongs.api.NetworkManager
import ru.atomarsoft.songstersongs.model.DataStatus
import ru.atomarsoft.songstersongs.model.Revision
import ru.atomarsoft.songstersongs.model.Song

class RevisionsActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_SONG_ID = "song_id"
        const val EXTRA_SONG_TITLE = "song_title"
        fun launch(activity: AppCompatActivity, song: Song) {
            val intent = Intent(activity, RevisionsActivity::class.java).apply {
                putExtra(EXTRA_SONG_ID, song.songId)
                putExtra(EXTRA_SONG_TITLE, song.title)
            }
            activity.startActivity(intent)
        }
    }

    private lateinit var revisionsListView: RecyclerView
    private lateinit var revisionListAdapter: RevisionAdapter
    private lateinit var statusTextView: TextView

    private lateinit var viewModel: RevisionsActivityModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)

        val viewModelProvider = ViewModelProvider(this)
        viewModel = viewModelProvider.get(RevisionsActivityModel::class.java)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.songId = intent.extras?.get(EXTRA_SONG_ID) as Int

        title = intent.extras?.get(EXTRA_SONG_TITLE) as String

        statusTextView = findViewById(R.id.commentsStatusText)
        revisionsListView = findViewById(R.id.recyclerComments)
        revisionListAdapter = RevisionAdapter(revisionsListView, viewModel.revisions)

        revisionsListView.visibility = View.GONE
        statusTextView.visibility = View.VISIBLE

        viewModel.status.observe(
            this,
            { state ->
                revisionListAdapter.notifyDataSetChanged()
                when (state) {
                    DataStatus.Loading -> {
                        statusTextView.text = getString(R.string.strLoading)
                        statusTextView.visibility = View.VISIBLE
                        revisionsListView.visibility = View.GONE
                    }
                    DataStatus.Ready -> {
                        statusTextView.visibility = View.GONE
                        revisionsListView.visibility = View.VISIBLE
                    }
                    DataStatus.Error -> {
                        statusTextView.text = getString(R.string.strErrorLoadingRevisions)
                        statusTextView.visibility = View.VISIBLE
                        revisionsListView.visibility = View.GONE
                    }
                    else -> {
                        statusTextView.text = getString(R.string.strInitializing)
                        revisionsListView.visibility = View.GONE
                    }
                }
            }
        )

        if (viewModel.status.value == DataStatus.None) reloadRevisions()
    }

    private fun reloadRevisions() {
        viewModel.status.value = DataStatus.Loading
        NetworkManager.getApiRequestResult(LoadRevisionsRequest(
            viewModel.songId,
            { list ->
                viewModel.revisions.clear()
                viewModel.revisions.addAll(list)
                viewModel.status.value = DataStatus.Ready
            },
            {
                viewModel.status.value = DataStatus.Error
            }
        ))
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

}