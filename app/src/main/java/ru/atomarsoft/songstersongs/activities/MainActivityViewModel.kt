package ru.atomarsoft.songstersongs.activities

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.atomarsoft.songstersongs.model.DataStatus
import ru.atomarsoft.songstersongs.model.Song

class MainActivityViewModel : ViewModel() {

    val status: MutableLiveData<DataStatus> = MutableLiveData(DataStatus.None)
    val songs: MutableList<Song> = mutableListOf()
    var searchString = ""
    var searchDialogIsShown = false

}
