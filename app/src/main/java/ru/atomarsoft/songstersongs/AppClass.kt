package ru.atomarsoft.songstersongs

import android.app.Application
import ru.atomarsoft.songstersongs.api.NetworkManager

class AppClass : Application() {

    override fun onCreate() {
        super.onCreate()
        NetworkManager.init(this)
    }

}