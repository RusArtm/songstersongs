package ru.atomarsoft.songstersongs.misc

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import kotlin.math.ceil
import kotlin.math.min

class DifficultyView : View {

    constructor(context: Context?) : super(context)

    constructor(
        context: Context?,
        attrs: AttributeSet?
    ) : super(context, attrs)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)

    private val density = context.resources.displayMetrics.density
    private val paint: Paint = Paint()
    private val count = 8
    private val elementWidth: Float = 4 * density
    private val gapWidth: Float = 4 * density
    private val elementHeight: Float = 8 * density

    private val activeColor = Color.parseColor("#8CA0B7")
    private val disabledColor = Color.parseColor("#D5D5D5")
    private val undefinedColor = Color.parseColor("#E5E5EB")

    private var value: Int = 3

    fun setValue(v: Int) {
        value = v
        invalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        val w =
            if (widthMode == MeasureSpec.AT_MOST) ceil(count * elementWidth + (count - 1) * gapWidth).toInt()
            else widthMeasureSpec
        val h =
            if (heightMode == MeasureSpec.AT_MOST || heightMode == MeasureSpec.UNSPECIFIED) ceil(
                elementHeight
            ).toInt()
            else heightMeasureSpec
        super.setMeasuredDimension(w, h)
    }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)

        if (canvas == null) return

        val w = (width - (gapWidth * (count - 1))) / count
        val h = height.toFloat()

        if (value > 0) {
            paint.color = activeColor
            for (i: Int in 0 until count) {
                if (i == value) paint.color = disabledColor
                canvas.drawRect(
                    i * (w + gapWidth),
                    0f,
                    i * (w + gapWidth) + w,
                    h,
                    paint
                )
            }
        } else {
            paint.strokeWidth = height - 4f
            paint.color = undefinedColor
            paint.strokeCap = Paint.Cap.ROUND
            val centerY = height / 2f
            val shiftX = min(width / 3f, centerY)
            canvas.drawLine(shiftX, centerY, width - shiftX, centerY, paint)
//            canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
        }
    }
}
