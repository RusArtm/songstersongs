package ru.atomarsoft.songstersongs.api

import org.json.JSONArray

abstract class ApiRequest<T: Any>(
    val succeed: (data: T) -> Unit,
    val fail: (message: String) -> Unit
) {

    abstract fun makeUrl(): String

    abstract fun handleResults(jsonArray : JSONArray): T

}