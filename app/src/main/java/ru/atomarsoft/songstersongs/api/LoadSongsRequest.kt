package ru.atomarsoft.songstersongs.api

import org.json.JSONArray
import ru.atomarsoft.songstersongs.model.Song

class LoadSongsRequest(
    private val pattern: String,
    private val count: Int,
    succeed: (data: List<Song>) -> Unit,
    fail: (message: String) -> Unit
) : ApiRequest<List<Song>>(
    succeed,
    fail
) {

    override fun makeUrl(): String {
        return "songs?size=" + count.toString() + (if (pattern == "") "" else "&pattern=$pattern")
    }

    override fun handleResults(jsonArray: JSONArray): List<Song> {
        val list: MutableList<Song> = mutableListOf()
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.optJSONObject(i)
            if (jsonObject != null) {
                val song = Song.jsonToSong(jsonObject)
                list.add(song)
            }
        }

        return list
    }

}