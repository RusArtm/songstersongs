package ru.atomarsoft.songstersongs.api

import org.json.JSONArray
import ru.atomarsoft.songstersongs.model.Revision

class LoadRevisionsRequest(
    private val songId: Int,
    succeed: (data: List<Revision>) -> Unit,
    fail: (message: String) -> Unit
) : ApiRequest<List<Revision>>(
    succeed,
    fail
) {

    override fun makeUrl(): String {
        return "meta/$songId/revisions"
    }

    override fun handleResults(jsonArray: JSONArray): List<Revision> {
        val list: MutableList<Revision> = mutableListOf()
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.optJSONObject(i)
            if (jsonObject != null) {
                val comment = Revision.jsonToRevision(jsonObject)
                list.add(comment)
            }
        }

        return list
    }

}