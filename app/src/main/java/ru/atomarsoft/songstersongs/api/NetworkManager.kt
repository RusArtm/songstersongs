package ru.atomarsoft.songstersongs.api

import android.content.Context
import android.os.Handler
import android.util.Log
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import kotlin.concurrent.thread
import kotlin.random.Random

object NetworkManager {
    private val base_url = "https://www.songsterr.com/api/"
    private var requestQueue: RequestQueue? = null

    private const val LOG_DEBUG_TAG = "DataLoading"
    private const val NOT_INITIALIZED_MESSAGE = "Request manage is not initialized yet"
    private var handler: Handler = Handler()

    /**
     * Set this delay to add some work to thread which performs request result handling
     */
    var threadDebugDelay = 1000L

    fun init(context: Context) {
        requestQueue = Volley.newRequestQueue(context)
    }

    private fun checkInitialized(): Boolean {
        return requestQueue != null
    }

    fun <T : Any> getApiRequestResult(apiRequest: ApiRequest<T>) {
        if (!checkInitialized()) {
            apiRequest.fail(NOT_INITIALIZED_MESSAGE)
            return
        }

        val url = apiRequest.makeUrl()

        val request = JsonArrayRequest(
            base_url + url,
            { jsonArray ->
                Log.d(LOG_DEBUG_TAG, "Response received from api")
                thread(start = true) {
                    if (threadDebugDelay > 0L) doSomeWork(threadDebugDelay)
                    Log.d(LOG_DEBUG_TAG, "Starting real response processing...")
                    val result = apiRequest.handleResults(jsonArray)
                    Log.d(LOG_DEBUG_TAG, "Response processing done.")
                    handler.post { apiRequest.succeed(result) }
                }
            },
            { error ->
                apiRequest.fail(error.message.toString())
            }
        )

        Log.d(LOG_DEBUG_TAG, "Request created and now will be added to queue...")
        requestQueue!!.add(request)
    }

    /**
     * This is just a delay function to make thread to do something for given duration
     * The calculation inside could be deleted with no harm to main purpose to the function
     */
    private fun doSomeWork(delay: Long) {
        Log.d(LOG_DEBUG_TAG, "Starting some work for $delay mls")
        val startTime = System.currentTimeMillis()

        val rnd = Random(startTime)
        var v: Double = rnd.nextDouble()
        while (System.currentTimeMillis() < startTime + delay) {
            if (v == Double.POSITIVE_INFINITY) v = rnd.nextDouble()
            v += v * 2
        }
        Log.d(LOG_DEBUG_TAG, "Some work is done with this result: $v")
    }

}
