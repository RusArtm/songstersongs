package ru.atomarsoft.songstersongs.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.atomarsoft.songstersongs.R
import ru.atomarsoft.songstersongs.model.Revision

class CommentViewHolder(view: View) : GeneralViewHolder<Revision>(view) {

    val summaryText: TextView = view.findViewById(R.id.listitemRevisionSummary)
    val dateAndPersonText: TextView = view.findViewById(R.id.listitemRevisionBy)

    override fun onUpdate() {
        val revision: Revision = currentObj ?: return
        summaryText.text = revision.summary
        dateAndPersonText.text = String.format(
            itemView.context.getString(R.string.strRevisionFromBy),
            revision.date,
            revision.personName
        )
    }

}

class RevisionAdapter(
    recyclerView: RecyclerView,
    list: List<Revision>
) : GeneralAdapter<Revision>(recyclerView, list) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GeneralViewHolder<Revision> {
        val view = inflater.inflate(R.layout.listitem_revision, parent, false)
        return CommentViewHolder(view)
    }

}
