package ru.atomarsoft.songstersongs.adapters

import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class GeneralViewHolder<T : Any>(view: View) : RecyclerView.ViewHolder(view) {

    interface ClickListener<T> {
        fun onClick(item: T)
    }

    protected var currentObj: T? = null
    private var clickListener: ClickListener<T>? = null

    init {
        itemView.setOnClickListener {
            if (clickListener == null || currentObj == null) return@setOnClickListener
            clickListener!!.onClick(currentObj!!)
        }
    }

    fun update(obj: T, clickListener: ClickListener<T>?) {
        currentObj = obj
        if (currentObj == null) return
        onUpdate()
        this.clickListener = clickListener
    }

    abstract fun onUpdate()
}

abstract class GeneralAdapter<T : Any>(
    recyclerView: RecyclerView,
    private val list: List<T>
) : RecyclerView.Adapter<GeneralViewHolder<T>>() {

    protected val inflater: LayoutInflater = LayoutInflater.from(recyclerView.context)
    private var clickListener: GeneralViewHolder.ClickListener<T>? = null

    fun setItemClickListener(listener: GeneralViewHolder.ClickListener<T>?) {
        clickListener = listener
        notifyDataSetChanged()
    }

    init {
        recyclerView.adapter = this
        recyclerView.layoutManager =
            LinearLayoutManager(recyclerView.context, RecyclerView.VERTICAL, false)
    }

    override fun onBindViewHolder(holder: GeneralViewHolder<T>, position: Int) {
        val item: T? = getItemByPosition(position)
        if (item != null) holder.update(item, clickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun getItemByPosition(position: Int): T? {
        return if (list.size > position && position >= 0) list[position]
        else null
    }

}
