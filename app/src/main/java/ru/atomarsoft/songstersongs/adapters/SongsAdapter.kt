package ru.atomarsoft.songstersongs.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.atomarsoft.songstersongs.R
import ru.atomarsoft.songstersongs.misc.DifficultyView
import ru.atomarsoft.songstersongs.model.Song

class SongViewHolder(view: View) : GeneralViewHolder<Song>(view) {
    private val songTitle: TextView = view.findViewById(R.id.listItemSongTitle)
    private val artistTitle: TextView = view.findViewById(R.id.listItemArtistTitle)
    private val difficultyView: DifficultyView = view.findViewById(R.id.listItemSongDifficulty)

    override fun onUpdate() {
        val obj: Song = currentObj ?: return
        songTitle.text = obj.title
        artistTitle.text = obj.artist
        difficultyView.setValue(obj.getDifficulty())
    }
}

class SongsAdapter(
    recyclerView: RecyclerView,
    songs: List<Song>
) : GeneralAdapter<Song>(recyclerView, songs) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GeneralViewHolder<Song> {
        val view = inflater.inflate(R.layout.listitem_song, parent, false)
        return SongViewHolder(view)
    }

}
