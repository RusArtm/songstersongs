package ru.atomarsoft.songstersongs.model

import org.json.JSONObject

data class Track(
    val instrumentId: Int,
    val tuningString: String,
    val difficulty: Int,
    val dailyViews: Int
) {
    companion object {
        fun jsonToTrack(json: JSONObject): Track {
            val instrumentId = json.optInt("instrumentId", 0)
            val tuningString = json.optString("tuningString", "")
            val difficultyStr = json.optString("difficulty", "")
            val dailyViews = json.optInt("dailyViews", 0)

            val difficulty = when (difficultyStr) {
                "VERY_EASY" -> 1
                "EASY" -> 2
                "BELOW_INTERMEDIATE" -> 3
                "INTERMEDIATE" -> 4
                "UPPER_INTERMEDIATE" -> 5
                "HARD" -> 6
                "VERY_HARD" -> 7
                "INSANE" -> 8
                else -> 0
            }

            return Track(
                instrumentId,
                tuningString,
                difficulty,
                dailyViews
            )
        }
    }
}
