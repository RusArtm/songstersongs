package ru.atomarsoft.songstersongs.model

enum class DataStatus {
    None,
    Loading,
    Ready,
    Error
}
