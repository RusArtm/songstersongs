package ru.atomarsoft.songstersongs.model

import org.json.JSONObject
import kotlin.math.round

data class Song(
    val songId: Int,
    val title: String,
    val hasChords: Boolean,

    val artistId: Int,
    val artist: String,

    val tracks: List<Track>
) {

    companion object {
        fun jsonToSong(jsonObject: JSONObject): Song {
            val songId = jsonObject.optInt("songId", 0)
            val title = jsonObject.optString("title", "")
            val hasChords = jsonObject.optBoolean("hasChords", false)
            val artistId = jsonObject.optInt("artistId", 0)
            val artist = jsonObject.optString("artist", "")

            val tracks = mutableListOf<Track>()
            val tracksJson = jsonObject.optJSONArray("tracks")
            if (tracksJson != null) {
                for (trackNo in 0 until tracksJson.length()) {
                    val track = Track.jsonToTrack(tracksJson.optJSONObject(trackNo))
                    tracks.add(track)
                }
            }
            return Song(songId, title, hasChords, artistId, artist, tracks)
        }
    }

    fun getDifficulty(): Int {
        val ratedTrackDifficulty =
            tracks
                .filter { track -> track.difficulty > 0 }
                .map { track -> track.difficulty }
        val res =
            if (ratedTrackDifficulty.isNotEmpty())
                round(
                    ratedTrackDifficulty.sum().toFloat() / ratedTrackDifficulty.size.toFloat()
                ).toInt()
            else
                0
        // Returning difficulty for songs with Id % 10 == 0 to demonstrate difficulty view empty
        return if (songId % 10 == 0) 0 else res
    }

}
