package ru.atomarsoft.songstersongs.model

import org.json.JSONObject

data class Revision(
    val id: Long,
    val personId: Long, // personId
    val personName: String, // person
    val date: String,
    val summary: String, // Summary
    val source: String
) {
    companion object {
        fun jsonToRevision(json: JSONObject): Revision {
            val id = json.optLong("id", 0)
            val personId = json.optLong("personId", 0L)
            val personName = json.optString("person", "")
            val dateStr = json.optString("date", "")
            val summary = json.optString("summary", "")
            val source = json.optString("source", "")

            return Revision(
                id,
                personId,
                personName,
                dateStr,
                summary,
                source
            )
        }
    }
}
